package com.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.drivermanager.DriverManager;

public class Admin_Qualification_Education {

	public static WebDriverWait wait;

	private static Admin_Qualification_Education admin_qualification_education_instance;

	public static Admin_Qualification_Education getInstance() {

		if (admin_qualification_education_instance == null) {

			admin_qualification_education_instance = new Admin_Qualification_Education();
		}
		return admin_qualification_education_instance;

	}

	@FindBy(id = "menu_admin_viewAdminModule")
	WebElement admin_Module_Locator;
	@FindBy(id = "menu_admin_Qualifications")
	WebElement qualification_Module_Locator;
	@FindBy(id = "menu_admin_viewEducation")
	WebElement education_Module_Locator;
	@FindBy(id = "btnAdd")
	WebElement addBtn_Locator;
	@FindBy(id = "education_name")
	WebElement name_Locator;
	@FindBy(id = "btnSave")
	WebElement saveBtn_Locator;
	@FindBy(xpath = "//a[contains(.,'B.E.,')]//preceding::td")
	WebElement checkBox_Locator;
	@FindBy(id = "btnDel")
	WebElement deleteBtn_Locator;

	public void education() {

		Actions act = new Actions(DriverManager.getDriver());
		act.moveToElement(admin_Module_Locator).perform();
		act.moveToElement(qualification_Module_Locator).perform();
		education_Module_Locator.click();

		addBtn_Locator.click();
		name_Locator.sendKeys("B.E.,");
		saveBtn_Locator.click();
		checkBox_Locator.click();
		deleteBtn_Locator.click();

	}

}
