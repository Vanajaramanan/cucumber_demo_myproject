package com.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.drivermanager.DriverManager;

public class Admin_Configuration_Email {

	public static WebDriverWait wait;
	
	

	private static Admin_Configuration_Email admin_email_confi_instance;

	public static Admin_Configuration_Email getInstance() {
		
		
		

		if (admin_email_confi_instance == null) {

			admin_email_confi_instance = new Admin_Configuration_Email();
		}
		return admin_email_confi_instance;

	}

	@FindBy(id = "menu_admin_viewAdminModule")
	WebElement admin_Module_Locator;
	@FindBy(id = "menu_admin_Configuration")
	WebElement configuration_Module_Locator;
	@FindBy(id = "menu_admin_listMailConfiguration")
	WebElement email_Configuration;
	@FindBy(id = "editBtn")
	WebElement editBtn_Locator;
	@FindBy(id = "emailConfigurationForm_txtMailAddress")
	WebElement from_Email;
	@FindBy(id = "emailConfigurationForm_chkSendTestEmail")
	WebElement sent_Mail_CheckBox;
	@FindBy(id = "emailConfigurationForm_txtTestEmail")
	WebElement to_Email;
	@FindBy(id = "editBtn")
	WebElement saveBtn_Locator;

	public void email_Subscription() {

		Actions act = new Actions(DriverManager.getDriver());
		act.moveToElement(admin_Module_Locator).perform();
		act.moveToElement(configuration_Module_Locator).perform();
		email_Configuration.click();
		editBtn_Locator.click();
		from_Email.clear();
		from_Email.sendKeys("xyz@gmail.com");
		sent_Mail_CheckBox.click();
		to_Email.clear();
		to_Email.sendKeys("abc@gmail.com");
		saveBtn_Locator.click();

	}

}
