package com.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.drivermanager.DriverManager;

public class Performance_Configure_Trackers {
	
	private static Performance_Configure_Trackers  Performance_Configure_Trackers_instance;

	public static Performance_Configure_Trackers getInstance() {

		if (Performance_Configure_Trackers_instance == null) {

			Performance_Configure_Trackers_instance = new Performance_Configure_Trackers();
		}
		return Performance_Configure_Trackers_instance;
		
	}

	@FindBy(xpath = "/html/body/div[1]/div[2]/ul/li[7]/a/b")
	WebElement performanceLocator;
	@FindBy(xpath = "//*[@id=\"menu_performance_Configure\"]")
	WebElement configurelocator;

	@FindBy(xpath = "//*[@id=\"menu_performance_addPerformanceTracker\"]")
	WebElement Trackerlocator;
	@FindBy(xpath = "//*[@id=\"btnAdd\"]")
	WebElement addbuttonlocator;
	@FindBy(xpath = "//*[@id=\"addPerformanceTracker_tracker_name\"]")
	WebElement Trackernamlocator;

	@FindBy(xpath = "//*[@id=\"addPerformanceTracker_employeeName_empName\"]")
	WebElement employeenamelocator;

	@FindBy(xpath = "/html/body/div[4]/ul/li[1]")
	WebElement Aliya;

	@FindBy(xpath = "//*[@id=\"addPerformanceTracker_availableEmp\"]")
	WebElement availablereviewerslocator;
	@FindBy(xpath = "//*[@id=\"btnAssignEmployee\"]")
	WebElement addbutnlocator;
	@FindBy(xpath = "//*[@id=\"btnSave\"]")
	WebElement savebttnlocator;

	@FindBy(xpath = " //td[contains(a,'Ramanan')]//preceding-sibling::td//input")
	WebElement checkboxloactor;
	@FindBy(xpath = "//*[@id=\"btnDelete\"]")
	WebElement deletbuttonlocator;
	@FindBy(xpath = "//*[@id=\"dialogDeleteBtn\"]")
	WebElement deletokbuttonlocator;

	public void addPerformanceTracker() {
		Actions act = new Actions(DriverManager.getDriver());
		act.moveToElement(performanceLocator).perform();
		act.moveToElement(configurelocator).perform();
		Trackerlocator.click();
		addbuttonlocator.click();
		Trackernamlocator.sendKeys("Ramanan");
		employeenamelocator.clear();
		employeenamelocator.sendKeys("Aliya");
		Aliya.click();

		Select available = new Select(availablereviewerslocator);
		available.selectByVisibleText("John Smith");
		addbutnlocator.click();
		savebttnlocator.click();
		checkboxloactor.click();
		deletbuttonlocator.click();
		deletokbuttonlocator.click();

	}

}
