package com.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.drivermanager.DriverManager;

public class Admin_organisation_Structure {
	

	public static WebDriverWait wait;

	private static Admin_organisation_Structure admin_organisation_structure_instance;

	public static Admin_organisation_Structure getInstance() {

		if (admin_organisation_structure_instance == null) {

			admin_organisation_structure_instance = new Admin_organisation_Structure();
		}
		return admin_organisation_structure_instance;

	}


	@FindBy(id = "menu_admin_viewAdminModule")
	WebElement admin_Module_Locator;
	@FindBy(id = "menu_admin_Organization")
	WebElement organization_Module_Locator;
	@FindBy(id = "menu_admin_viewCompanyStructure")
	WebElement structure_Locator;
	@FindBy(id = "btnEdit")
	WebElement editBtn_Locator;
	@FindBy(xpath = "//a[contains(.,'TechOps')]//following::a")
	WebElement addStructure_Locator;
	@FindBy(name = "txtUnit_Id")
	WebElement unitId_Locator;
	@FindBy(id = "txtName")
	WebElement nameField_Locator;
	@FindBy(id = "txtDescription")
	WebElement descriptionField_Locator;
	@FindBy(id = "btnSave")
	WebElement saveBtn_Locator;
	@FindBy(xpath = "//a[contains(.,'TechOps')]//following::a//following::a")
	WebElement deleteStructure_Locator;
	@FindBy(id = "dialogYes")
	WebElement deleteConfirmBtn;

	public void structure() {
		Actions act = new Actions(DriverManager.getDriver());
		act.moveToElement(admin_Module_Locator).perform();
		act.moveToElement(organization_Module_Locator).perform();
		structure_Locator.click();
		editBtn_Locator.click();
		addStructure_Locator.click();
		unitId_Locator.sendKeys("123");
		nameField_Locator.sendKeys("Testing");
		descriptionField_Locator.sendKeys("automation testing");
		saveBtn_Locator.click();
		deleteStructure_Locator.click();
		WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(), 10);
		wait.until(ExpectedConditions.visibilityOf(deleteConfirmBtn));
		deleteConfirmBtn.click();

	}

}
