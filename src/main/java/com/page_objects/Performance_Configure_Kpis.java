package com.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.drivermanager.DriverManager;

public class Performance_Configure_Kpis {

	private static Performance_Configure_Kpis Performance_Configure_Kpis_instance;

	public static Performance_Configure_Kpis getInstance() {

		if (Performance_Configure_Kpis_instance == null) {

			Performance_Configure_Kpis_instance = new Performance_Configure_Kpis();
		}
		return Performance_Configure_Kpis_instance;

	}

	@FindBy(xpath = "/html/body/div[1]/div[2]/ul/li[7]/a/b")
	WebElement performanceLocator;
	@FindBy(xpath = "//*[@id=\"menu_performance_Configure\"]")
	WebElement configurelocator;
	@FindBy(xpath = "//*[@id=\"menu_performance_searchKpi\"]")
	WebElement kpislocator;
	@FindBy(xpath = "//*[@id=\"btnAdd\"]")
	WebElement addbuttonlocator;
	@FindBy(id = "defineKpi360_jobTitleCode")
	WebElement jobtitlelocator;
	@FindBy(xpath = "//*[@id=\"defineKpi360_keyPerformanceIndicators\"]")
	WebElement keyperformanceindicatorlocator;
	@FindBy(xpath = "//*[@id=\"defineKpi360_minRating\"]")
	WebElement minimunmratinglocator;
	@FindBy(xpath = "//*[@id=\"defineKpi360_maxRating\"]")
	WebElement maximumratinglocator;
	@FindBy(xpath = "//*[@id=\"defineKpi360_makeDefault\"]")
	WebElement checkboxlocator;
	@FindBy(xpath = "//*[@id=\"saveBtn\"]")
	WebElement savebuttonlocator;
	@FindBy(xpath = "//*[@id=\"kpi360SearchForm_jobTitleCode\"]")
	WebElement jobtitlesearchlocator;
	@FindBy(xpath = "//*[@id=\"searchBtn\"]")
	WebElement searchbttnlocator;
	@FindBy(xpath = "//td[contains(a,'Ramanan')]//preceding-sibling::td//input")
	WebElement checkboxlocatorfordelet;
	@FindBy(xpath = "//*[@id=\"btnDelete\"]")
	WebElement deletbuttonlocator;
	@FindBy(xpath = "//*[@id=\"dialogDeleteBtn\"]")
	WebElement deletokbuttonlocator;

	public void addKeyPerformanceIndicator() {

		Actions act = new Actions(DriverManager.getDriver());
		act.moveToElement(performanceLocator).perform();
		act.moveToElement(configurelocator).perform();
		kpislocator.click();
		addbuttonlocator.click();

		Select selct = new Select(jobtitlelocator);
		selct.selectByValue("18");
		keyperformanceindicatorlocator.sendKeys("Ramanan");
		minimunmratinglocator.sendKeys("5");
		maximumratinglocator.clear();

		maximumratinglocator.sendKeys("8");
		checkboxlocator.click();
		savebuttonlocator.click();
		// Select searchjobtitle = new Select(jobtitlesearchlocator);
		// searchjobtitle.selectByVisibleText("Automation Tester");
		searchbttnlocator.click();
		checkboxlocatorfordelet.click();
		deletbuttonlocator.click();
		deletokbuttonlocator.click();
	}
}
