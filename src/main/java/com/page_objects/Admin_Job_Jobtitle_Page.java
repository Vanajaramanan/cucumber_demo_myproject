package com.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.drivermanager.DriverManager;



public class Admin_Job_Jobtitle_Page{
	

	public static WebDriverWait wait;

	private static Admin_Job_Jobtitle_Page Admin_Jobtitle_instance;



	public static Admin_Job_Jobtitle_Page getInstance() {

		if (Admin_Jobtitle_instance == null) {

			Admin_Jobtitle_instance = new Admin_Job_Jobtitle_Page();
		}
		return Admin_Jobtitle_instance;

	}

	@FindBy(id="menu_admin_viewAdminModule") WebElement admin_Module_Locator;
	@FindBy(id="menu_admin_Job") WebElement job_Module_Locator;
	@FindBy(id="menu_admin_viewJobTitleList") WebElement job_title_Locator;
	@FindBy(id="btnAdd") WebElement addBtn_Locator;
	@FindBy(id="jobTitle_jobTitle") WebElement job_title_Txtfield;
	@FindBy(id="jobTitle_jobDescription") WebElement job_Description_Field;
	@FindBy(id="jobTitle_note") WebElement job_Note_Field;
	@FindBy(id="btnSave") WebElement saveBtn_Locator;
	@FindBy(xpath="//a[contains(text(),'Selenium Tester')]/../preceding-sibling::td//input") 
	WebElement checkBox_Locator;
	@FindBy(id="btnDelete") WebElement deleteBtn_Locator;
	@FindBy(id="dialogDeleteBtn")  WebElement deleteBtn_Dialog_Locator;
	
	
	
	public void jobTitle() {
	Actions	act = new Actions(DriverManager.getDriver());
		act.moveToElement(admin_Module_Locator).perform();
		act.moveToElement(job_Module_Locator).perform();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		job_title_Locator.click();
		addBtn_Locator.click();
		job_title_Txtfield.sendKeys("Selenium Tester");
		job_Description_Field.sendKeys("Test automation");
		job_Note_Field.sendKeys("Website Testing");
		saveBtn_Locator.click();
		
		WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(),10);
		wait.until(ExpectedConditions.visibilityOf(checkBox_Locator));
		checkBox_Locator.click();
		deleteBtn_Locator.click();
		wait.until(ExpectedConditions.visibilityOf(deleteBtn_Dialog_Locator));
		deleteBtn_Dialog_Locator.click();
		
		

	}
	
	
}
