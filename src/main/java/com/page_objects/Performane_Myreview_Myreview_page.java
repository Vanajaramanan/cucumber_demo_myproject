package com.page_objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.drivermanager.DriverManager;

public class Performane_Myreview_Myreview_page {

	private static Performane_Myreview_Myreview_page Performane_Myreview_Myreview_instance;

	public static Performane_Myreview_Myreview_page getInstance() {

		if (Performane_Myreview_Myreview_instance == null) {
			Performane_Myreview_Myreview_instance = new Performane_Myreview_Myreview_page();
		}
		return Performane_Myreview_Myreview_instance;
	}

	@FindBy(xpath = "/html/body/div[1]/div[2]/ul/li[7]/a/b")
	WebElement performanceLocator;
	@FindBy(xpath = "/html/body/div[1]/div[2]/ul/li[7]/ul/li[2]/a")
	WebElement manageReviewLocator;
	@FindBy(xpath = "//*[@id=\"menu_performance_myPerformanceReview\"]")
	WebElement myreviewlocator;
	// @FindBy(xpath="//*[@id=\"resultTable\"]/tbody/tr/td[1]/a")WebElement
	// userclicklocator;

	public void toCheckUser() {
		Actions act = new Actions(DriverManager.getDriver());
		act.moveToElement(performanceLocator).perform();

		act.moveToElement(manageReviewLocator).perform();
		myreviewlocator.click();
		// userclicklocator.click();
	}

}
