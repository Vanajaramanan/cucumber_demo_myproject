package com.page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.drivermanager.DriverManager;



public class Leave_LeaveList {
	
	
	
	private static Leave_LeaveList  Leave_LeaveList_instance;

	public static Leave_LeaveList getInstance() {

		if (Leave_LeaveList_instance == null) {

			Leave_LeaveList_instance = new Leave_LeaveList();
		}
		return Leave_LeaveList_instance;

	}
	@FindBy(id = "menu_leave_viewLeaveModule")
	WebElement leaveLocator;
	@FindBy(xpath = "//*[@id=\"menu_leave_viewLeaveList\"]")
	WebElement leavelistlocator;
	@FindBy(xpath = "//*[@id=\"leaveList_chkSearchFilter_checkboxgroup\"]")
	WebElement pendinglocator;

	@FindBy(xpath = "(//input[@type='checkbox'])")
	WebElement allCheckBox;
	@FindBy(id = "calFromDate")
	WebElement fromDate;
	@FindBy(xpath = "//*[@id=\"frmFilterLeave\"]/fieldset/ol/li[1]/img")
	WebElement fromCalender;
	@FindBy(name = "leaveList[calToDate]")
	WebElement toDate;
	@FindBy(xpath = "//*[@id=\"frmFilterLeave\"]/fieldset/ol/li[2]/img")
	WebElement toCalender;
	@FindBy(id = "btnReset")
	WebElement resetBtn;



	public void leaveList() {

		Actions action = new Actions(DriverManager.getDriver());
		action.moveToElement(leaveLocator).perform();
		leavelistlocator.click();
		pendinglocator.click();

		// Deselecct all the CheckBox

		for (int i = 1; i < 7; i++) {

			WebElement checkBox = DriverManager.getDriver().findElement(By.xpath("(//input[@type='checkbox'])[" + i + "]"));
			checkBox.click();
			if (allCheckBox.isSelected() == false) {
			}
		}
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//		// Select all the checkBox
//
//		for (int j = 6; j < 7; j++) {
//			WebElement checkBox1 = driver.findElement(By.xpath("(//input[@checked='checked'])[" + j + "]"));
//			checkBox1.click();
//		}
//		if (allCheckBox.isSelected() == true) {
//			System.out.println("Checkbox validation is done");
//		}

		// date

		String fromDateValue = fromDate.getAttribute("value");
		fromDate.click();
		fromDate.clear();
		fromDate.sendKeys("2021-08-01");
		fromCalender.click();

		String toDateValue = toDate.getAttribute("value");
		toDate.click();
		toDate.clear();
		toDate.sendKeys("2021-11-11");
		toCalender.click();

		// Reset the Date

		resetBtn.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Date validation

		String fromDateValue1 = DriverManager.getDriver().findElement(By.xpath("//*[@id=\"calFromDate\"]")).getAttribute("value");
		String toDateValue1 = DriverManager.getDriver().findElement(By.xpath("//*[@id=\"calToDate\"]")).getAttribute("value");
		if (fromDateValue.equalsIgnoreCase(fromDateValue1)) {
			System.out.println("From Date Resetted");
		}
		if (toDateValue.equalsIgnoreCase(toDateValue1)) {
			System.out.println("To date Resetted");
		}

	}
}
