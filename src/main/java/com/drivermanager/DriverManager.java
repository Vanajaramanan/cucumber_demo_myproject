package com.drivermanager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.constants.Constants;
import com.step_definitions.Common_Step_Definition;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DriverManager {

	private static WebDriver driver = null;
	private static final Logger LOGGER = LogManager.getLogger(Common_Step_Definition.class.getName());

	public static WebDriver getDriver() {
		return driver;
	}
	// this method initialize and launch the browser
	public static void launchBrowser() {
		try {

			switch (Constants.BROWSER) {
			case "chrome":
				WebDriverManager.chromedriver().setup();

				LOGGER.info("Lauching" + Constants.BROWSER);
				driver = new ChromeDriver();
				driver.manage().window().maximize();
				break;

			default:
				System.setProperty("webdriver.chrome.driver", "C:\\workspace\\Framework\\Input\\chromedriver97.exe");
				driver = new ChromeDriver();
				break;
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

}
