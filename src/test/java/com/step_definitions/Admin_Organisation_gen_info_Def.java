package com.step_definitions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

import com.page_objects.Admin_organisation_General_Information_Page;
import com.utils.CommonUtils;

import io.cucumber.java.en.Given;

public class Admin_Organisation_gen_info_Def {
	
	private static final Logger LOGGER = LogManager.getLogger(Admin_Organisation_gen_info_Def.class.getName());

	@Given("^Admin can able to update the general information to  organisation$")
	public void admin_can_able_to_update_the_general_information_to_organisation() throws Throwable {
	    
		try {

	Admin_organisation_General_Information_Page.getInstance().general_Information();
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error(" Admin organisation general information are failed ");
			
			Assert.fail();

		}

	}
		
		
	
	}
