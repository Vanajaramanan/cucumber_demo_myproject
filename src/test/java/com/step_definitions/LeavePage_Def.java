package com.step_definitions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.page_objects.LeavePage;

import io.cucumber.java.en.Given;

public class LeavePage_Def {
	private static final Logger LOGGER = LogManager.getLogger(LeavePage_Def.class.getName());

	
	
	@Given("^To  validate  checkbox and Reset validation$")
	public void to_validate_checkbox_and_reset_validation() {
		
		try {
			 LeavePage.getInstance().myLeave();
			
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error(" user can t able to leave assign ");
		}
	 
	}
}
