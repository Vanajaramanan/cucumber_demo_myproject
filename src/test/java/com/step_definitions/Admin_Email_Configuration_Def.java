package com.step_definitions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

import com.page_objects.Admin_Configuration_Email;
import com.utils.CommonUtils;
import io.cucumber.java.en.Given;

public class Admin_Email_Configuration_Def {

	 private static final Logger LOGGER =
	 LogManager.getLogger(Admin_Email_Configuration_Def.class.getName());
	 
	


	@Given("^admin email configuration  scenario$")
	public void admin_email_configuration_scenario() throws Throwable {

		try {
			 Admin_Configuration_Email.getInstance().email_Subscription();
			
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error("Admin Email Configuration ");
		
			Assert.fail();

		}

	}

}
