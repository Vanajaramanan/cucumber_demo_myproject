package com.step_definitions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

import com.page_objects.Admin_Qualification_Language_page;
import com.utils.CommonUtils;
import io.cucumber.java.en.Given;
public class Admin_Qualification_languagge_Def {

	private static final Logger LOGGER = LogManager.getLogger(Admin_Qualification_languagge_Def.class.getName());

	@Given("^Admin can able to check qualification language$")
	public void admin_can_able_to_check_qualification_language() throws Throwable {

		try {
			Admin_Qualification_Language_page.getInstance().language();

		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error(" Admin can able to chcek qualification");
		
			Assert.fail();

		}

	}
}
