package com.step_definitions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

import com.page_objects.Performance_Configure_Kpis;
import com.page_objects.Performance_Configure_Trackers;

import io.cucumber.java.en.Given;

public class Performance_Configure_Trackers_Def {

	private static final Logger LOGGER = LogManager.getLogger(Performance_Configure_Trackers_Def.class.getName());

	@Given("^To check performance trackers$")
	public void to_check_performance_trackers() {

	

		try {
			Performance_Configure_Trackers.getInstance().addPerformanceTracker();

		} catch (Exception e) {
			// TODO: handle exception

			LOGGER.error(" user can t able to kpi  assign ");

			Assert.fail();
		}

	}
}


