package com.step_definitions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

import com.page_objects.Admin_Qualification_License;
import com.utils.CommonUtils;

import io.cucumber.java.en.Given;

public class Admin_Qualification_Liscence_Def {

	private static final Logger LOGGER = LogManager.getLogger(Admin_Qualification_Liscence_Def.class.getName());

	@Given("^admin able check quallifcation liscence$")
	public void admin_able_check_quallifcation_liscence() throws Throwable {
		try {
			Admin_Qualification_License.getInstance().license();

		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.error(" Admin can able to chcek qualification Liscenece");
		
			Assert.fail();

		}

	}
}
