package com.step_definitions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

import com.page_objects.Performance_Configure_Trackers;
import com.page_objects.Performane_Myreview_Myreview_page;

import io.cucumber.java.en.Given;

public class Performance_Myreview_Def {
	private static final Logger LOGGER = LogManager.getLogger(Performance_Myreview_Def.class.getName());

	@Given("^To check My review$")
	public void to_check_my_review() {

		try {
			Performane_Myreview_Myreview_page.getInstance().toCheckUser();
		} catch (Exception e) {
			// TODO: handle exception

			LOGGER.error(" user can t able to kpi  assign ");

			Assert.fail();
		}

	}
}
