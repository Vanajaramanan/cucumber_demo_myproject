package com.runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		
		 plugin = {
		 "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:","rerun:target/failed_scenario.txt"
		 },
		 features = "src/test/resources/features",
		 glue = "com.step_definitions",
		 dryRun = false,
		 monochrome = true,
		 tags =" @performance"
//
//		plugin = { "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:",
//				"rerun:target/failed_scenario.txt" },
//		features = "src/test/resources/performace.feature",
//		glue = "com.step_definitions", dryRun = false, monochrome = true,
//		tags = "@performance"
		 )

public class TestRunner {

}
